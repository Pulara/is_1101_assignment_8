#include <stdio.h>

struct student{
    char fName[20];
    char subject[15];
    int marks;
};

struct student stu[6];

int main()
{
    printf("This program is to read and display some details of students.\n\n");
    getDetails();
    printf("\n--------------- Displaying student details ------------------\n\n");
    displayDetails();

    return 0;
}

void getDetails(){
    for(int i = 0; i < 6; i++ ){
        printf("\n-------------- Enter the details of student %d ---------------\n",i+1);
        printf("First name : ");
        scanf("%s",stu[i].fName);
        printf("Subject : ");
        scanf("%s",stu[i].subject);
        printf("Marks : ");
        scanf("%d",&stu[i].marks);
    }
}

void displayDetails(){
    for(int j = 0; j < 6; j++ ){
        printf("\nStudent %d --------------------------------------------------- \n",j+1);
        printf("\tFirst name : %s\n",stu[j].fName);
        printf("\tSubject : %s\n",stu[j].subject);
        printf("\tMarks : %d\n",stu[j].marks);
    }
}
